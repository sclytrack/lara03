<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\TodosController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
Route::get('/', function () {
    return view('welcome');
});
*/

Auth::routes();

//Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

//Adding the ? makes it optional.

//Route::get('/overzicht/{datum?}', [TodosController::class, 'overzicht']);

Route::controller(TodosController::class)->group( function() {
  Route::get('/', 'overzicht');
  Route::get('/home', 'overzicht');
  Route::get('/overzicht/{datum?}', 'overzicht');
  Route::get('/todo/{todoID?}/{datum?}', 'todo');
  Route::get('/uitgevoerd/{datum?}', 'uitgevoerd');
  Route::get('/update/{actie?}/{todoID}/{datum?}', 'update');
  Route::post('/bewaar','bewaar');
  //Mail stuff ajax
  Route::post('/jxMailTodoLijst', 'mailTodoLijst');
});