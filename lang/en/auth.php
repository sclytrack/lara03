<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'De gegevens vinden we niet terug.',
    'password' => 'Het opgegeven wachtwoord is fout.',
    'throttle' => 'Teveel aanmeldingen. Wacht :seconds seconden vooraleer opnieuw te proberen.',

];
