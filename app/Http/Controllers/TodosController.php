<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth; 
use Validator;
use Carbon\Carbon; // datatime thingy from laravel

use App\Models\Todo;
use App\Http\Middleware\Kalender;
use Mail;
use App\Mail\MailTodoLijst;




//App/Http/Controllers/TodosController

class TodosController extends Controller
{
    public function __construct()
    {
        $this->_oKalender = new Kalender();
        $this->_oTodos = new Todo();
    }

    //
    public function overzicht(Request $request, $datum='')
    {
        // is de gebruiker niet aangemeld?
        if (!Auth::check())
        {
            return redirect('/login');
        }

        //Maak de sessievariable 'uitgevoerd' als die niet bestaat.

        if (!$request->session()->has('uitgevoerd'))
        {
            $request->session()->put('uitgevoerd', false);
        }



        // parameters voor view

        $dta = [
          'datumVandaag' => $this->_oKalender->vandaag()->toDateString()
        ];

        // datum object

        $oDatum = $this->_oKalender->valideerDatum($datum);
        $dta['datum'] = $oDatum->format('d-m-Y');
        $dta['datumString'] = $this->_oKalender->datumString($oDatum);

        $dta['datumNu'] = $oDatum->toDateString();


        $oCopy = clone $oDatum;
        $dta['datumVorige'] = $oCopy->addDays(-1)->toDateString();
        $dta['datumVolgende'] = $oCopy->addDays(2)->toDateString();

        // Kalender rechter kolom.
        $dta['kalender'] = $this->_oKalender->kalender($oDatum);

        //todos

        //???
        $dta['todos'] = $this->_oTodos->dag($dta['datumNu'], $request->session()->get('uitgevoerd'));



        $dta['uitgevoerd'] = $request->session()->get('uitgevoerd');


        return view('pagina.dag')->with($dta);
    }


    // toggle todo uitgevoerd

    public function todo($todoID=0, $datum='')
    {
        if ($todoID > 0)
          $this->_oTodos->todoUitgevoerd($todoID);

        return redirect("/overzicht/{$datum}"); //dubbel aanhalingsteken verplicht?
    }

    /**
     * Toggle uitgevoerde todo's weergeven
     */

    public function uitgevoerd(Request $request, $datum = '')
    {
        $request->session()->put('uitgevoerd', !$request->session()->get('uitgevoerd'));
        return redirect("/overzicht/{$datum}");
    }

    /**
     * formulier weergeven nieuw|bewerk|verwijder
     */

    public function update(Request $request, $actie='', $todoID=0, $datum='', $fouten = false)
    {
        // niet aangemeld --> redirect naar login
        if (!Auth::check())
        {
            return redirect('/login');
        }

        // datum-object
        $oDatum = $this->_oKalender->valideerDatum($datum);

        // controlleer de actie
        $actie=strtolower(trim($actie));

        $actie = in_array($actie, ['bewerk', 'nieuw', 'verwijder']) ? $actie: 'nieuw';

        $dta = [
            'actie' => $actie,
            'fouten' => $fouten
        ];

        // indien actie bewerk of berwijder, is todo van gebruiker?

        if ($actie != 'nieuw' && !$this->_oTodos->todoGebruiker($todoID)) //todoID added?
        {
            return redirect(sprintf('/overzicht/%s', $oDatum->format('Y-m-d')));
        }

        $dta['datum'] = $oDatum->format('Y-m-d'); 
        $dta['datumString'] = $this->_oKalender->datumString($oDatum);
        $dta['datumNu'] = $oDatum->toDateString();


        if ($actie == 'nieuw')
        {
            $dta['todo'] = $this->_oTodos->nieuw($dta['datumNu']);
        } else
        {
            $dta['todo'] = $this->_oTodos->todo($todoID);
        }

        return view('pagina.update')->with($dta);

    }



    //If action is 'verwijder' removes it from the database, if the validation fails it goes to update



    public function bewaar(Request $request) 
    {
        $actie = $request->input('actie');
        $todoID = $request->input('todoID');
        $datum = $request->input('datum');

        if ($actie == 'verwijder')
        {
            $this->_oTodos->verwijder($todoID);
            return redirect("/overzicht/{$datum}");  //dubbele aanhalingstekens.
        }

        // valideren formulier. Heb het laten draaien en valideer zelf doet niet veel.

        $valideer = Validator::make(
            $request->all(), 
            [
                'titel' => 'required | min:2',
                'beschrijving' => 'required | min:2'
            ],
            [
                'titel.required' => 'Titel is een verplicht veld',
                'titel.min' => 'Titel moet 2 of meer karakters bevatten',
                'beschrijving.required' => 'Titel is een verplicht veld',
                'beschrijving.min' => 'Titel moet 2 of meer karakters bevatten',
            ]
        );

        //het schrijft het resultaat in valideer

        if ($valideer->fails())
        {
            return $this->update($request, $actie, $todoID, $datum, $valideer->messages());
        }
        else
        {   
            //Store everything in the database
            $this->_oTodos->bewaar($request);
            $datum = $request->input('datum');
            return redirect("/overzicht/{$datum}");
        }
    }

    //mail stuff with ajax?

    public function mailTodoLijst(Request $request)
    {
        $json = [
            'success'=> false,
            'boodschap'=> ''
        ];

        //Get information from todo and kalender

        $datum = $request->input('datum');
        
    
        $oDatum = $this->_oKalender->valideerDatum($datum);

        $dta = [
            'naam' => Auth::user()->name,
            'datum' => $datum, 
            'datumString' => $this->_oKalender->datumString($oDatum),
            'lijst' => $this->_oTodos->dag($datum, false),
        ];

        // Control : for comments
        // print("<pre>");
        // print_r($dta);
        // print("</pre>");

        try {
            Mail::to(Auth::user()->email)->send(new MailTodoLijst($dta));
            $json['success'] = true;
        }
        catch(Exception $ex)
        {
            $json['boodschap'] = $ex->getMessage();
        }
        return response()->json($json);
    }  
}
