<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;


use Carbon\Carbon;

class Kalender
{

    private $_maanden = [1=>'januari', 'februari', 'maart', 'april', 'mei', 'juni', 'juli', 'augustus', 'september', 'oktober', 'november', 'december'];

    private $_dagen= ['zondag', 'maandag', 'dinsdag', 'woensdag', 'donderdag', 'vrijdag', 'zaterdag'];


    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        return $next($request);
    }

    /**
     * levert datum van vandaag
     */

    public function vandaag()
    {
        return Carbon::now(env('APP_TIJDZONE'));
    }


    /**
     * valideer datum URL
     * 
     * @param datum in de vorm van yyyy-mm-dd
     * @return datumobject
     */

     public function valideerDatum($datum='')
     {
        try {
          $tmp = explode('-', $datum);
          $oDatum = Carbon::createFromDate($tmp[0], $tmp[1], $tmp[2], env('APP-TIJDZONE'));
        } catch(\Throwable $th)
        {
            $oDatum = Carbon::now(env('APP_TIJDZONE'));
        }
        return $oDatum;
     }

     /**
     * datumString: zet datumobject om in datumtekst 
     * @param $oDatum datumObject
      */

    public function datumString($oDatum)
    {
      return sprintf('%s, %s %s %s',
      $this->_dagen[$oDatum->dayOfWeek], $oDatum->day,
      $this->_maanden[$oDatum->month], $oDatum->year
      );
    }

    public function kalender($oDatum)
    {
      $kalender = [
        'kalender' => [],
        'maand' => sprintf('%s %s', $this->_maanden[$oDatum->month], $oDatum->year),
        //'prefix' => $oDatum->format('Y-m-d'),
        'prefix' => $oDatum->format('Y-m-'),
        'dagen' => $this->_dagen
      ];

      //vorige | volgende maand
      $oCopy = clone $oDatum;

      $kalender['volgende'] = $oCopy->addMonths(1)->format('Y-m-d');
      $kalender['vorige'] = $oCopy->subMonths(2)->format('Y-m-d');

      // begin en einde maand


      //var_dump($oDatum);

      

      //$maandStart = new Carbon('first day of this month');
      $maandStart = $oDatum->startOfMonth();
      $eersteDagWeek = $maandStart->dayOfWeek;   //0 is Sunday, 1 is Monday, 2 is Tuesday, 3 Wednesday
      $maandEinde = $oDatum->endOfMonth();

                                                 //1 is Sunday, 2 is Monday, 3 is Tuesday.


      $rij = [];

      # Debugging
//      array_push($rij, $oDatum->toDateString());
//      array_push($rij, $maandStart->toDateString());
//      array_push($rij, $maandEinde->toDateString());

//      array_push($rij, $maandStart->day);       //30
//      array_push($rij, $maandStart->month);
//      array_push($rij, $maandStart->year);
//     array_push($rij, $maandEinde->day);       //30

      //end debugging.

      while(count($rij) < $eersteDagWeek)
        array_push($rij, '');

      for ($d = 1; $d <= $oDatum->endOfMonth()->day; $d++)
      {
        //dag aan de rij toevoegen
        $rij[] = $d; //array_push($rij, $d);

        // rij volledig -> voeg toe aan kalender, begin nieuwe rij
        if (count($rij) == 7)
        {
          array_push($kalender['kalender'], $rij);
          $rij = [];
        }
      }



      if (count($rij) > 0)
        while(count($rij) < 7) array_push($rij, '');
      array_push($kalender['kalender'], $rij);

      

      return $kalender;
    }


}
