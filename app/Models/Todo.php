<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Http\Middleware\Kalender;


use Auth;

//use Apps\Http\Middleware\Authenticate;



class Todo extends Model
{
    use HasFactory;

    /**
     * Methode dag levert lijst met todos voor datum (gekoppeld aan gebruiker)
     */

     public function dag($datum, $uitgevoerd=false)
     {
        $userID = Auth::user()->id; //->name ->email
        //query
        $todos = self::where('userID', '=', $userID)->where('datum', '=', $datum);

        if (!$uitgevoerd)
        {
            $todos = $todos->where('uitgevoerd', '=', false);
        }

        $todos = $todos->orderBy('tijdstip')->get();

        return $todos;  
     }

     public function todoUitgevoerd($todoID)
     {
         $todo = self::find($todoID);
         $todo->uitgevoerd = 1 - $todo->uitgevoerd;
         $todo->update();
 
     }


     /***
      * Controleert of todoItem gekoppeld is aan een gebruiker
      */
    public function todoGebruiker($todoID)
    {
        return self::where('id', '=', $todoID)->where('userID', '=', Auth::user()->id)->count();
    }

    /**
     * Nieuw, leeg todoitem
     */
    
    public function nieuw($datum)
    {
        $kalender = new Kalender();
        $oDatum = $kalender->valideerDatum($datum);

        return [
            'id' => 0,
            'userID' => Auth::user()->id,
            'datum' => $datum, 
            'tijdstip' => '06:00',
            'titel' => '',
            'beschrijving' => '',
            'level' => 0,
            'uitgevoerd' => 0,
            'datumString' => $kalender->datumString($oDatum)
        ];
    }


     /**
     * inhoud todo ophalen
     */
    
    public function todo($todoID)
    {
        //  ophalen rij todo
        $todo = self::find($todoID);

        $kalender = new Kalender();
        $oDatum = $kalender->valideerDatum($todo->datum);

        return [
            'id' => $todoID,
            'userID' => Auth::user()->id, // = $todo->userID
            'datum' => $todo->datum,
            'tijdstip' => $todo->tijdstip,
            'titel' => $todo->titel,
            'beschrijving' => $todo->beschrijving,
            'level' => $todo->level,
            'uitgevoerd' => $todo->uitgevoerd,
            'datumString' => $kalender->datumString($oDatum)
        ];
    }


    /**
     * verwijder todo-item
     */

    public function verwijder($todoID)
    {
      self::find($todoID)->delete();   //delete the Id when found from the database.
    }    


    //Insert or update
    public function bewaar($request)
    {
        $actie = $request->input('actie');
        $todoID = $request->input('todoID');
        $userID = $request->input('userID');
        $titel = $request->input('titel');
        $beschrijving = $request->input('beschrijving');
        $datum = $request->input('datum');
        $tijdstip = $request->input('tijdstip');
        $level = $request->input('level');
        $uitgevoerd = $request->input('uitgevoerd');

        if ($actie == 'nieuw')
        {
            $todo = new Self();
            
        
        } else
        { //update
            $todo = self::find($todoID);
            
        }

        $todo->userID = $userID;
        $todo->titel = $titel;
        $todo->beschrijving = $beschrijving;
        $todo->datum = $datum;
        $todo->tijdstip = $tijdstip;
        $todo->level = $level;
        $todo->uitgevoerd = $uitgevoerd;


        $todo->save();
        /*
        if ($actie == 'nieuw')
        {
          $todo->save();
        } else
        {
          $todo->update();
        }
        */
        
    }
}
