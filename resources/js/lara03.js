$(() => {
    if ('form') FORMULIER.init();

    //dag.blade.php
    if ('#mailLijst') MAIL.init();
});


const MAIL = {

    oAJX: null,
    timeout: 6000, //6 seconden

    init: () => {


        //<meta name="csrf-token" content="{{ csrf_token() }}" from app.blade.php>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"').attr('content')
            }
        });


        $('#btnMail').on('click', MAIL.todoLijst);
    },

    //on button click make visibility hidden. Using ajax.
    todoLijst: () =>
    {
        $('#btnMail').css('visibility', 'hidden');
        let datum = $('#btnMail').data('datum');
        //alert(datum);
        let frmDta = new FormData();   //kind of array
        frmDta.append('datum', datum);

        MAIL.oAJX = $.ajax({
            url: '/jxMailTodoLijst',
            type: 'POST',
            dataType: 'json',
            data: frmDta, 
            processData: false, //browser does not check content
            contentType: false, //???
            cache: false, //may not be cached
            success: MAIL.succes, //wat doen als success
            error: MAIL.fout, //wat doen als het fout gaat
            timeout: MAIL.timeout  //6 seconden verstreken.
        })
    },

    succes: (jqDta) =>
    {
        MAIL.oAJX = null; //destroy object

        let boodschap='test';

        //console.log(jqDta);

        if (jqDta.success)
        {
            boodschap = $('<div>').addClass('alert alert-info')
                .append($('<h4>').addClass('alert-heading').text('Todo lijst'))
                .append($('<p>').html('Todo lijst werd met <strong>SUCCES</strong> verstuurd...'))
                .append('<hr>')
                .append($('<p>').html(jqDta.boodschap));
        } else
        {
            boodschap = $('<div>').addClass('alert alert-warning')
                .append($('<h4>').addClass('alert-heading').text('Todo lijst'))
                .append($('<p>').html('Todo lijst mailen <strong>MISLUKT</strong>'))
                .append('<hr>')
                .append($('<p>').html(jqDta.boodschap));            
        }
        $('#mailLijst').prop('class', 'mb-3').html('').append(boodschap);
    },

    fout: (jqXHR, jqMsg) => //xmlHttpRequest, jQuery message
    {
        MAIL.oAJX.abort();
        MAIL.oAJX = null; //destroy object
    },

    timeout: () =>
    {

    },
}


const FORMULIER = {
    maanden: ['januari', 'februari', 'maart', 'april', 'mei', 'juni',
        'juli', 'augustus', 'september', 'oktober', 'november', 'december'],
    dagen: ['zondag', 'maandag', 'dinsdag',
        'woensdag', 'donderdag', 'vrijdag', 'zaterdag'],

    init: () => {
        $('#btnAnnuleer').on('click', FORMULIER.annuleer);
        if ($('#actie').val() !== 'verwijder') FORMULIER.kalender();

        $('#uur, #min').on('change', FORMULIER.tijd);
        $('#btnBewaar').on('click', FORMULIER.bewaar);
    },

    bewaar: () =>{
        $('form').submit();
    },

    annuleer: () => {
        let datum = $('#datum').val();
        window.location = `/overzicht/${datum}`;
    },

    kalender: () => {
        $.fn.datepicker.defaults.autoclose = true;
        let datum = $('#datum').val();

        $('#datum').datepicker({
            language: 'nl-BE',
            format: 'yyyy-mm-dd'
        }).on('changeDate', FORMULIER.updateDatum);
    },

    updateDatum: () => {
        let datum = $('#datum').val();
        let oDatum = new Date(datum);  //create an date object from a string
        datum = datum.split('-');
        let datumString = `${FORMULIER.dagen[oDatum.getDay()]}, ${datum[2]} ${FORMULIER.maanden[oDatum.getMonth()]} ${datum[0]}`; //franse aanhalingstekens.
        $('#datumString').val(datumString);
    },

    tijd: () => {
      let uur = $('#uur').val();
      let min = $('#min').val();
      let tijd = `${uur}:${min}`;
      $('#tijdstip').val(tijd);
    },
}


// Control-F5 browser clears the cash
// F2 in explorer to rename file.
