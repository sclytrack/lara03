<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.bunny.net/css?family=Nunito" rel="stylesheet">

    <!-- Scripts -->
    <link rel="stylesheet" href="/js/datejs/css/bootstrap-datepicker.min.css">

    <!--Please note this is in the public/js-->

    <script src ="{{ URL::to('js/jquery.js')}}"></script>
    <script src="{{ URL::to('js/datejs/js/bootstrap-datepicker.min.js') }}"></script>
    <!-- this one in the resources -->
    @vite(['resources/js/app.js'])
</head>
<body>
    <div id="app">
        @include('include.kop')

        <main class="py-4">
            @yield('inhoud')
        </main>

        @include('include.voet')
    </div>
</body>
</html>
