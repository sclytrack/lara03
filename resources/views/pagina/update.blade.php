@extends('layouts.app')

@section('inhoud')
    <div class="container">
        <!-- class="container-fluid" takes the entire width-->
        <div class="row">
            <h2>
                @switch($actie)
                    @case('nieuw')
                        Nieuw todo
                    @break

                    @case('bewerk')
                        Todo bewerken
                    @break

                    @case('verwijder')
                        Kill todo
                    @break

            
                @endswitch
            </h2>

            @if($fouten)
              <div class="alert alert-warning">
                <h4 class="alert-heading">Fouten:</h4>
                <ul>
                    @foreach($fouten->all() as $fout)
                      <li>{{ $fout }}</li>
                    @endforeach
                </ul>
              </div>
            @endif


            <form action="/bewaar" method="post">
                @csrf
                <!-- ajax uses the id and the post uses the name, just give it the same name-->
                <input type="hidden" id="todoID" name="todoID" value="{{ $todo['id'] }}">
                <input type="hidden" id="userID" name="userID" value="{{ $todo['userID'] }}">
                <input type="hidden" id="actie" name="actie" value="{{ $actie }}">
                <input type="hidden" id="tijdstip" name="tijdstip" value="{{ $todo['tijdstip'] }}">

                <div class="row">
                    <?php
                    $disabled = $actie == 'verwijder' ? 'disabled' : '';
                    ?>

                    <!--linkerkant-->


                    <div class="col-sm-6 col-xs-12">
                        <div class="mb-3">
                            <label for="titel" class="form-label">Titel</label>
                            <input type="text" class="form-control" name="titel" id="titel"
                                value="{{ $todo['titel'] }}" {{ $disabled }}>
                        </div>
                        <div class="mb-3">
                            <label for="beschrijving" class="form-label">Beschrijving</label>
                            <textarea class="form-control" id="beschrijving" name="beschrijving" {{ $disabled }}>{{ $todo['beschrijving'] }}
                            </textarea>
                        </div>
                    </div>

                    <!--rechterkant-->


                    <div class="col-sm-6 col-xs-12">
                        <div class="mb-3" id="frmDatum">
                            <label for="" class="form-label">Datum</label>
                            <div id="frmDatum">
                                <input type="text" class="form-control" name="datum" id="datum"
                                    value="{{ $todo['datum'] }}">
                                <input type="text" class="form-control" name="datumString" id="datumString"
                                    value="{{ $todo['datumString'] }}" {{ $disabled }}>
                            </div>
                        </div>
                        <div class="mb-3 mt-5">
                            <!--margen top-->
                            <label for="" class="form-label">Tijdstip</label>
                            <?php
                            
                            $tijd = explode(':', $todo['tijdstip']);
                            $uur = $tijd[0];
                            $min = $tijd[1];
                            ?>

                            <div class="row">
                                <div class="col-5">
                                    <select class="form-select" name="uur" id="uur" {{ $disabled }}>
                                        <?php
                                        foreach (range(0, 23, 1) as $uurItem) {
                                            $selected = $uur == $uurItem ? 'selected' : '';
                                            printf('<option value="%02d" %s>%02d</option>', $uurItem, $selected, $uurItem);
                                        }
                                        ?>

                                    </select>
                                </div>

                                <div class="col-5">
                                    <select class="form-select" name="min" id="min" {{ $disabled }}>
                                        <?php
                                        foreach (range(0, 55, 5) as $minItem) {
                                            $selected = $min == $minItem ? 'selected' : '';
                                            printf('<option value="%02d" %s>%02d</option>', $minItem, $selected, $minItem);
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>

                            <div class="mb-3">
                                <p>Level</p>
                                <div class="btn-group">
                                    <input type="radio" class="btn-check" name="level" id="level0" value="0"
                                        @if ($todo['level'] == 0) checked @endif {{ $disabled }}>
                                    <label for="level0" class="btn btn-outline-primary">Standaard</label>

                                    <input type="radio" class="btn-check" name="level" id="level1" value="1"
                                        @if ($todo['level'] == 1) checked @endif {{ $disabled }}>
                                    <label for="level1" class="btn btn-outline-secondary">Aanbevolen</label>

                                    <input type="radio" class="btn-check" name="level" id="level2" value="2"
                                        @if ($todo['level'] == 2) checked @endif {{ $disabled }}>
                                    <label for="level2" class="btn btn-outline-warning">Belangrijk</label>

                                    <input type="radio" class="btn-check" name="level" id="level3" value="3"
                                        @if ($todo['level'] == 3) checked @endif {{ $disabled }}>
                                    <label for="level3" class="btn btn-outline-danger">Dringend</label>
                                </div>
                            </div>

                            <div class="mb-3">
                                <p>Uitgevoerd</p>
                                <div class="btn-group">
                                    <input type="radio" class="btn-check" name="uitgevoerd" id="uitgevoerd0"
                                        value="0" @if ($todo['uitgevoerd'] == 0) checked @endif
                                        {{ $disabled }}>
                                    <label for="uitgevoerd0" class="btn btn-outline-primary">Uitgevoerd</label>

                                    <input type="radio" class="btn-check" name="uitgevoerd" id="uitgevoerd1"
                                        value="1" @if ($todo['uitgevoerd'] == 1) checked @endif
                                        {{ $disabled }}>
                                    <label for="uitgevoerd1" class="btn btn-outline-secondary">Uitgevoerd</label>

                                </div>
                            </div>

                        </div>

                    </div>

                    <!-- End Two columns -->
                    <hr>

                    <!--start bottom block-->
                    <div class="row">
                        <div class="col-12 text-end">
                            <button type="button" class="btn btn-primary" id="btnBewaar">
                                <i class="bi bit-check-square"></i>
                                @if ($actie == 'verwijder')
                                    Verwijder
                                @else
                                    Bewaar
                                @endif
                            </button>
                            <button type="button" class="btn btn-secondary" id="btnAnnuleer">
                                <i class="bi bi-x-square"></i>
                                Annuleer
                            </button>
                        </div>
                    </div>
                    <!--end bottom block-->

                </div>
                <!-- end row block -->
            </form>

        </div>
    </div>
@endsection
