@extends('layouts.app')

@section('inhoud')
    <div class="container" id="todoDag">
        <div class="row">
            <div class="col-6">
                <div class="row">
                    <a href="/overzicht/{{ $datumVorige }}" class="btn btn-primary">
                        <i class="bi bi-chevron-double-left"></i>
                    </a>
                    <h2>{{ $datumString }}</h2>
                    <a href="/overzicht/{{ $datumVolgende }}" class="btn btn-primary">
                        <i class="bi bi-chevron-double-right"></i>
                    </a>
                </div>

                <hr>

                <div class="row">
                    @if (count($todos) > 0)
                        <div id="mailLijst" class="mb-3 text-end">   <!-- margin bottom 3  text at end-->
                            <button type="button" class="btn btn-primary" id="btnMail" data-datum="{{ $datumNu }}">
                                <i class="bi bi-envolope-fill"></i> <!-- bootstrap icon-->
                                Mail todo lijst
                            </button>

                        </div>

                        @foreach ($todos as $todo)
                            <?php
                            $levels = ['secondary', 'primary', 'warning', 'danger'];
                            $level = sprintf('alert-%s', $levels[$todo->level]);
                            ?>

                            <div class='alert {{ $level }}'>
                                <div class="float-end custom-control custom-switch">
                                    <?php
                                        $ico = $todo->uitgevoerd ? 'bi-check-square-fill': 'bi-app';
                                    ?>

                                    <a href="/todo/{{ $todo->id }}/{{ $datumNu }}" class="btn">
                                        <i class="bi {{ $ico }}"></i>
                                    </a>

                                </div>

                                <h4 class="alert-heading">
                                    {{ $todo->tijdstip }} &nbsp; {{ $todo->titel }}
                                </h4>
                                <div>
                                    {{ $todo->beschrijving }}
                                </div>
                                <hr>
                                <div class="btn-group float-end">
                                    <a href="/update/bewerk/{{ $todo->id }}/{{ $datumNu }}"
                                        class="btn btn-primary">
                                        <i class="bi bi-pencil-square"></i>Bewerk
                                    </a>
                                    <a href="/update/verwijder/{{ $todo->id }}/{{ $datumNu }}"
                                        class="btn btn-warning">
                                        <i class="bi bi-x-square"></i>Verwijder
                                    </a>
                                </div>
                            </div>
                        @endforeach
                    @else
                        <div class="alert alert-success">
                            <h4 class="alert-heading">
                                Vandaag... geen todo's
                            </h4>
                        </div>
                    @endif
                </div>

            </div>
            <div class="col-5 offset-1">
                <!--Switch to 5 column and start at offset 1-->
                <div class="row mb-3">
                    <!--margin bottom 3-->
                    <div class="btn-group">
                        <?php
                        $btnUitgevoerd = $uitgevoerd ? 'btn-primary' : 'btn-light';
                        ?>
                        <a href="/uitgevoerd/{{ $datumNu }}" class="btn {{ $btnUitgevoerd }}">
                            <i class="bi bi-check-square">
                                Uitgevoerd
                            </i>
                        </a>
                        <a href="/overzicht/{{ $datumVandaag }}" class="btn btn-light">
                            <i class="bi bi-calendar-date">
                                Vandaag
                            </i>
                        </a>
                        <a href="/update/nieuw/0/{{ $datumNu }}" class="btn btn-light">
                            <i class="bi bi-plus-square">
                                Nieuw
                            </i>
                        </a>

                    </div>
                </div>

                <div class="row">
                    <a href="/overzicht/{{ $kalender['vorige'] }}" class="btn btn-primary">
                        <i class="bi bi-chevron-double-left"></i>
                    </a>
                    <h2>{{ $kalender['maand'] }}</h2>
                    <a href="/overzicht/{{ $kalender['volgende'] }}" class="btn btn-primary">
                        <i class="bi bi-chevron-double-right"></i>
                    </a>
                </div>
                <div id="dagKalender">
                    <div class="row">
                        @foreach (['zo', 'ma', 'di', 'wo', 'do', 'vr', 'za'] as $dag)
                            <a href="#" class="btn">{{ $dag }}</a>
                        @endforeach
                    </div>

                    @foreach ($kalender['kalender'] as $rij)
                        <div class="row">
                            @foreach ($rij as $dag)
                                @if (empty($dag))
                                    <a href="#" class="btn"
                                        onclick="function(evt) { evt.preventDefault();}">.</a>
                                @else
                                    <a href="/overzicht/{{ $kalender['prefix'] }}{{ $dag }}"
                                        class="btn btn-secondary">{{ $dag }}</a>
                                @endif
                            @endforeach
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection
