<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Todolijst voor {{ $dta['datumString'] }}</title>
</head>
<body>
    <div class="container">
        <div class="row">
            <h2>
                <small>Beste {{ $dta['naam'] }}</small><br>
                ToDo-lijst voor {{ $dta['datumString'] }}
            </h2>
            <hr style="border-color: silver;">
            @foreach ($dta['lijst'] as $todoItem)
              <h3>
                  {{ $todoItem['tijdstip'] }}: &nbsp; {{ $todoItem['titel'] }} &nbsp;
                  @php
                    foreach(range(0, intval($todoItem['level'])) as $i)
                    {
                        print("\u{2605}");
                    }
                  @endphp
              </h3>
              <p>
                {{ $todoItem['beschrijving'] }}
              </p>
              <hr style="border-color: silver;">
            @endforeach
        </div>
    </div>
</body>
</html>